#!/bin/bash
start=`date +%s`

# Script assumes your devstack is already running with regular DBs and needs to be restarted into clean mode
echo "Stopping appserver"
cd ~/src/appserver
~/src/appserver/stop_appserver.sh
echo "Restarting devstack"
cd ~/docker
~/docker/start_devstack -e cleandb

if [ ! "$(docker ps | grep kafka)" ]; then
	echo "Kafka didn't start, restarting..."
	docker restart kafka tomcat-vs tomcat-db tomcat-sr2 mule mule3 epost
fi

echo "Starting appserver"
cd ~/src/appserver
~/src/appserver/start_appserver.sh

dockerGatewayIP=`docker network inspect bridge | grep Gateway | sed s/'"'/''/g | awk '{print $NF}'`
while :
do
	echo waiting for mule3 to start...
	if [ -z "`nc -w 1 -vz $dockerGatewayIP 9084 |& awk '{print $NF}' | grep '^open\|^succeeded!'`" ];
	then 
		sleep 10
	else
		break
	fi
done

read -p "Please add leaders manually [press enter to continue]"

echo "Running cleandb schema migrators"
SCHEMAS=(vs authdb securitydb fulfillmentdb paymentdb notificationdb ecommerce dataloader schedulerdb dwdb datawarehouse2 epost)
for schema in ${SCHEMAS[@]} ; do
    docker exec tomcat-vs /apps/schemamanager/bin/SchemaManager.sh --system-test-clean -s $schema
done

echo "Running VS system tests"
cd ~/src/vs/system-test
mvn test -Dtest=AutoShipTestSuite,BanfieldTestSuite,EconnectTestSuite,ExternalPimTestSuite,OpenApiTestSuite,ShippingFreeTestSuite,ShippingSurchargeTestSuite,SitecoreTestSuite

echo "Running appserver system tests"
cd ~/src/appserver/appserver-system-test/
~/src/appserver/appserver-system-test/run_integration_tests.sh dev

end=`date +%s.%N`
runtime=$( echo "$end - $start" | bc -l )
echo "CleanDB restart and system tests took $runtime seconds to run"