# sfrazee-vs-scripts

## clean_logs
Remove all the old dockerlogs!

## update_zoom
Zoom doesn't autoupdate on Linux, this gets the latest .deb file and installs it over the old one

## update_postman
The postman snap is slow. Updating the non-snap version is a pain, since you just download the binary. This makes it easier.  
If you need to create a desktop shortcut: https://learning.postman.com/docs/getting-started/installation-and-updates/#installing-postman-on-linux

