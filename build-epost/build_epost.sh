#!/bin/bash

if [ ! -s /usr/local/java6/bin/java ]
then
    echo "This script expects jdk 6 to be installed in /usr/local/java6"
    exit 0
fi

JAVA_HOME=/usr/local/java6

chmod +x projects/ansHealthConsole/console/build/build.sh
chmod +x projects/ansHealthB2B/b2b/build/build.sh
projects/ansHealthConsole/console/build/build.sh `pwd` /opt/Tomcat50 jboss_deploy
projects/ansHealthB2B/b2b/build/build.sh `pwd` /opt/jboss-4.0.1 jboss

