#!/bin/bash

responseNo="n"

echo "This script will copy jboss.tar and Tomcat50.tar from the current"
echo "directory to /opt and extract them.  It also copies java6.tar from"
echo "the current directory to /usr/local, extracts it, and create a symlink"
echo "from /usr/local/jdk1.6.0_17 to /usr/local/java6.  It must be run as root"
echo
echo -n "Continue?: [y/N]"

read response

if [ -z "$response" ]
then 
   response=$responseNo
fi

if [ $response = "N" -o  $response = "n" ]
then
   echo "Exiting"
   exit 0
fi

echo "You said yes"


mv jboss.tar /opt
mv Tomcat50.tar /opt
mv java6.tar /usr/local

cd /opt
tar xf jboss.tar
chmod -R 777 /opt/jboss*
rm -f jboss.tar

tar xf Tomcat50.tar
chmod -R 777 /opt/Tomcat*
rm -f Tomcat50.tar

cd /usr/local
tar xf java6.tar
ln -s /usr/local/jdk1.6.0_17 java6
rm -f java6.tar

