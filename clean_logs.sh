#!/bin/bash
#List the files to be deleted
find ~/dockerlogs/ -regex ".*log.*" -type f -mtime +2
# Actually delete them
find ~/dockerlogs/ -regex ".*log.*" -type f -mtime +2 -delete

# Rotate tomcat-db catalina.out
rm -v ~/dockerlogs/tomcat-db/catalina.out.old >> /dev/null
mv -v ~/dockerlogs/tomcat-db/catalina.out ~/dockerlogs/tomcat-db/catalina.out.old
