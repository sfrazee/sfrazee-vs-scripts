#!/bin/bash
# copy pick tickets from epost container
for f in $(docker exec -it epost bash -c "ls /pickticketprint/archive/vetsource3/supertote-*.pdf 2>/dev/null");
do
    docker cp epost:`echo $f | sed 's/\r//g'` ~/Desktop;
done
# copy summary from mule container
for f in $(docker exec -it mule bash -c "ls /pickticketprint/archive/vetsource3/supertote-*.pdf 2>/dev/null");
do
    docker cp mule:`echo $f | sed 's/\r//g'` ~/Desktop;
done
ls -1 ~/Desktop/supertote-*.pdf