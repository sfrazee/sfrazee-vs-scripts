#!/bin/bash
echo "Running VS system tests"
cd ~/src/vs/system-test
mvn test -Dtest=AutoShipTestSuite,BanfieldTestSuite,EconnectTestSuite,ExternalPimTestSuite,OpenApiTestSuite,ShippingFreeTestSuite,ShippingSurchargeTestSuite,SitecoreTestSuite

echo "Running appserver system tests"
cd ~/src/appserver/appserver-system-test/
~/src/appserver/appserver-system-test/run_integration_tests.sh dev